\titlepage

\begin{columns}[T]
    \column{.4\textwidth}
        \includegraphics[width=0.8\textwidth]{images/debian}
    \column{.1\textwidth}
        \begin{center}
            \includegraphics[width=1\textwidth]{images/pnlug}
        \end{center}
    \column{.4\textwidth}
        \begin{center}
            \raggedleft \small Linux Arena 2015
        \end{center}
\end{columns}

--------------------

\begin{center}
\Huge Debian? Linux?
\end{center}

Linux
-----

\begin{columns}
    \column{.40\linewidth}
        \includegraphics[height=1\textwidth]{images/tux}
        \begin{flushleft}
            \tiny Image by Larry Ewing, Simon Budig, Anja Gerwinski
            \tiny \href{https://commons.wikimedia.org/wiki/File:Tux.svg}{
                via Wikimedia Commons}
        \end{flushleft}
    \column{.50\linewidth}
        \begin{itemize}
            \item Cuore del vostro sistema
            \item è quella cosa che ha fatto esplodere il free software
        \end{itemize}
\end{columns}

Debian GNU/Linux
----------------

\begin{columns}
    \column{.30\linewidth}
        \includegraphics[height=1\textwidth]{images/debian}
    \column{.60\linewidth}
        \begin{itemize}
            \item Nato nel 1993 da Ian Murdock
            \item Primo vero rilascio nel 1996
            \item basato su \textbf{dpkg}
        \end{itemize}
\end{columns}

Il progetto: Il Social Contract
-------------------------------

Quel documento che descrive come il progetto Debian si approccia al Free Software

 1. **Debian will remain 100% free** (as in speech)
 2. **We will give back to the free software community**
 3. **We will not hide problems**
 4. **Our priorities are our users and free software**
 5. **Works that do not meet our free software standards**

\centering \href{https://www.debian.org/social\_contract}{https://www.debian.org/social\_contract}

Il progetto: le DFSG
--------------------

**Debian Free Software Guidelines**: la nostra concezione di Free Software

 1. Free Redistribution
 2. Source Code
 3. Derived Works
 4. Integrity of The Author's Source Code
 5. No Discrimination Against Persons or Groups
 6. No Discrimination Against Fields of Endeavor
 7. Distribution of License
 8. License Must Not Be Specific to Debian
 9. License Must Not Contaminate Other Software
 10. Example Licenses

I pacchetti
----------------------

 * Ogni singola cosa è un **pacchetto**
   * applicazioni
   * librerie
   * configurazioni di base
   * l'installer
 * Quello che i Debian Developer (e i contributori) fanno è creare e
   mantenere questi pacchetti.

Il modello di sviluppo
----------------------

\centerline{\xymatrix{
    upstream \ar[rr]|{packaging} &       & package  \ar[d]|{upload} \\
    unstable \ar[d]|{migration}  &       & incoming \ar[ll]|{checks} \\
    testing  \ar[rr]|{freeze}    &       & stable   \ar[dl] \\
                                 & users & }}

Una vista all'interno dei pacchetti 1/3
---------------------------------------

 * ogni pacchetto si porta dietro il proprio file di copyright
   * prima di entrare in archivio un pacchetto viene attentamente controllato
     per verifiarne la licenza. Un file non libero non arriverà mai su stable

Una vista all'interno dei pacchetti 2/3
---------------------------------------

 * seguiamo **FHS**, nessuna sorpresa nella posizione dei file sul file system

Una vista all'interno dei pacchetti 3/3
---------------------------------------

 * a parte rare eccezioni, dietro ogni pacchetto c'è una persona, detta
   Maintainer, o a volte un intero team.

L'archivio
----------

Debian ha un altissimo numero di pacchetti nel proprio archivio, correntemente
ci sono **21735** pacchetti sorgente in unstable


Questo vuol dire circa **40000** pacchetti che possono essere installati con un semplice `apt install $pkg`



-----------

\begin{center}
\Huge Questions?!?
\end{center}

Do you want more cookies?
-------------------------

 1. [https://www.debian.org](https://www.debian.org)
 2. [debian-italian@lists.debian.org](mailto:debian-italian@lists.debian.org):
    mailing list della comunità italiana di Debian
 3. [#debian-it @ OFTC](irc://irc.oftc.net/#debian-it): canale IRC

Thanks 1
--------

 * Copyrights/Licenses
   * The Debian Open Use Logo(s) are Copyright (c) 1999 Software in the Public
     Interest, Inc., and are released under the terms of the GNU Lesser
     General Public License, version 3 or any later version, or, at your
     option, of the Creative Commons Attribution-ShareAlike 3.0 Unported
     License.
   * The theme used for this slide is Copyright 2014-2015 Holger Levsen and
     Jérémy Bobbio, and is licensed under Creative Commons
     Attribution-ShareAlike 4.0 International License

Thanks 2
------

 * [PN LUG](http://www.pnlug.it), per l'organizzazione
 * **TU**, per essere venuto
 * \href{https://kernel.org}{Linux} per essere così divertente
 * [Debian](https://www.debian.org) per essere così pratico

\vspace{\fill}
\begin{flushright}
\begin{columns}
    \column{50px}
        \href{http://creativecommons.org/licenses/by-sa/4.0/}{
        \includegraphics[width=50px]{images/cc-by-sa}
        }
    \column{.70\linewidth}
        \footnotesize This work is licensed under a
        \href{http://creativecommons.org/licenses/by-sa/4.0/}{
        Creative Commons Attribution-ShareAlike 4.0 International License}.
\end{columns}
\end{flushright}

\centering
\begin{footnotesize}
\begin{tabular}{ l c }
    \hline
    email & \texttt{mattia@mapreri.org} \\
    GPG key & \texttt{66AE 2B4A FCCF 3F52 DA18  4D18 4B04 3FCD B944 4540} \\
    \hline
\end{tabular}
\end{footnotesize}
