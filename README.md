## Building

Unless otherwise specified in the readme, a simple `make` while you are in the
directory should do the trick.

## Copyright

Unless otherwise specified all the works in this repository is
Copyright © 2015 Mattia Rizzolo <mattia@mapreri.org>

## License

Everything in this repository, unless otherwise specified, is licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License. For the full
license text see https://creativecommons.org/licenses/by-sa/4.0/legalcode
