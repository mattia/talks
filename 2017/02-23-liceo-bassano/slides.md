\titlepage

\begin{columns}[T]
    \column{.4\textwidth}
        \centering \includegraphics[width=0.4\textwidth]{images/debian}
    \column{.1\textwidth}
        \centering \includegraphics[width=1\textwidth]{images/ubuntuit}
    \column{.1\textwidth}
        \centering \includegraphics[width=1\textwidth]{images/ubuntu} \\
        \centering \includegraphics[width=1\textwidth]{images/ubuntu-name}
    \column{.4\textwidth}
        \raggedleft \small Liceo Scientifico \\
        \raggedleft \small Jacopo da Ponte
\end{columns}


----

\begin{center}
\huge Ciclo di vita di un software Open Source
\end{center}

----

\includegraphics[width=1.2\paperwidth]{images/stream}

----

\begin{columns}
    \column{.5\textwidth}
        \centering \Large UPSTREAM
    \column{.5\textwidth}
        \centering \includegraphics[width=1\textwidth]{images/stream}
\end{columns}

----

\centerline{\xymatrix{
    upstream \ar[d]|{packaging} & \\
    distribuzioni GNU/Linux \ar[d]{} & \\
    \Large utenti & \\
}}

----

\begin{center}
\Huge Che vuol dire sviluppare una distribuzione GNU/Linux?
\end{center}

I Pacchetti
-----------

Mattoni del sistema, che è formato un insieme di pacchetti.

É quello che gli utenti ricevono da noi.

\hfill

\begin{columns}
    \column{.5\paperwidth}
        \centering \includegraphics[width=.3\textwidth]{images/package}
    \column{.5\paperwidth}
        \centering \includegraphics[width=.5\textwidth]{images/bricks}
\end{columns}

I Pacchetti
-----------

Ogni singola cosa è un **pacchetto**

  * applicazioni
  * librerie
  * configurazioni di base
  * l'installer
  
Quello che i sviluppatori di fatto fanno è creare e mantenere questi pacchetti.

----

\Large Sorgenti & Binari

----

\centering\Huge Cosa vuol dire sviluppare Ubuntu

----

Ubuntu è una **derivata** di _[Debian](https://debian.org)_

Debian GNU/Linux
----------------

\begin{columns}
    \column{.30\linewidth}
        \includegraphics[height=1\textwidth]{images/debian}
    \column{.60\linewidth}
        \begin{itemize}
            \item Nato nel 1993 da Ian Murdock
            \item Primo vero rilascio nel 1996
            \item basato su \textbf{dpkg}
        \end{itemize}
\end{columns}

----

Per confronto, il primo rilascio di Ubuntu è stato nel 2004.


Il modello di sviluppo di Debian
--------------------------------

\centerline{\xymatrix{
    upstream \ar[rr]|{packaging} &       & package  \ar[d]|{upload} \\
    unstable \ar[d]|{migration}  &       & incoming \ar[ll]|{checks} \\
    testing  \ar[rr]|{freeze}    &       & stable   \ar[dl] \\
                                 & users & }}

Il Maintainer
-------------

A parte rare eccezioni, dietro ogni pacchetto c'è una persona reale, detta
*Maintainer*, o a volte un intero team.

Il Maintainer
-------------

Compiti del maintainer:

 * ricevere bug dagli utenti finali, e gestirli
 
Il Maintainer
-------------

Compiti del maintainer:

 * ricevere bug dagli utenti finali, e gestirli
 * tenere sotto controllo upstream (nuove release, etc.)

Il Maintainer
-------------

Compiti del maintainer:

 * ricevere bug dagli utenti finali, e gestirli
 * tenere sotto controllo upstream (nuove release, etc.)
 * mantenere il pacchetti in buono stato, specialmente che non presenti bug
   critici o impedisca ad altri pacchetti di funzionare correttamente

----

\begin{center}
    \textit{.. Back to}
    \begin{figure}[h]
        \includegraphics[width=.4\textwidth]{images/ubuntu-name}
    \end{figure}
\end{center}

Ciclo di sviluppo di Ubuntu
---------------------------

\begin{columns}
    \column{.85\paperwidth}
        \centerline{\xymatrix{
            upstream \ar[d]|{packaging} & \\
            Debian unstable \ar[d]|{sync or merge} & \\
            Ubuntu devel \ar[d]|{freeze} & \\
            Ubuntu stable \ar[d] & \\
            users
        }}
    \column{.15\paperwidth}
        \includegraphics[width=1\textwidth]{images/kitten}
\end{columns}

Ubuntu development: sync/merge
------------------------------

 * **sync**: il pacchetto viene copiato da Debian in Ubuntu così com'è
 * **merge**: il pacchetto subisce delle modifiche

Ubuntu development: merge
-------------------------

Una volta che una differenza viene introdotta tra Debian e Ubuntu, bisogna
ri-applicarla ogni volta che c'è una nuova release in Debian (fintanto che non
viene applicata upstream

Ubuntu development
------------------

Ubuntu ha alcuni software unici, come il Software Center, Unity, ...

 

Vanno sviluppati :)

Ubuntu development
------------------

Ed ha anche obbiettivi diversi da Debian da cui deriva: vuole essere semplice
ed immediata da usare

 

 

 
 => differenti configurazioni di default

Le dimensioni
-------------

Debian ha un altissimo numero di pacchetti nel proprio archivio, correntemente
ci sono **25833** pacchetti sorgente in unstable


Questo vuol dire più di **50000** pacchetti che possono essere installati con
un semplice `apt install $pkg`


-----------

\centering \Huge Questions?!?

Thanks 1
--------

 * Copyrights/Licenses
   * The Ubuntu logo is Copyright © Canonical Ltd.
   * The Debian Open Use Logo(s) are Copyright (c) 1999 Software in the Public
     Interest, Inc., and are released under the terms of the GNU Lesser
     General Public License, version 3 or any later version, or, at your
     option, of the Creative Commons Attribution-ShareAlike 3.0 Unported
     License.
   * All the other images are available in an free license (either GFDL or
     CC-BY-SA) from Wikipedia; copyright of their respective owners.

Thanks 2
--------

 * Il Liceo, per averci invitato
 * **TU**, per essere venuto ed aver ascoltato fino a qui
 * \href{https://kernel.org}{Linux} per essere così divertente
 * [Debian](https://www.debian.org) per essere così pratico
 * [Ubuntu](https://ubuntu.com) per darmi qualcosa da dare alle persone normali

\vspace{\fill}
\begin{flushright}
\begin{columns}
    \column{50px}
        \href{http://creativecommons.org/licenses/by-sa/4.0/}{
        \includegraphics[width=50px]{images/cc-by-sa}
        }
    \column{.70\linewidth}
        \footnotesize This work is licensed under a
        \href{http://creativecommons.org/licenses/by-sa/4.0/}{
        Creative Commons Attribution-ShareAlike 4.0 International License}.
\end{columns}
\end{flushright}

\centering
\begin{footnotesize}
\begin{tabular}{ l c }
    \hline
    email & \texttt{mattia@mapreri.org} \\
    GPG key & \texttt{66AE 2B4A FCCF 3F52 DA18  4D18 4B04 3FCD B944 4540} \\
    \hline
\end{tabular}
\end{footnotesize}
