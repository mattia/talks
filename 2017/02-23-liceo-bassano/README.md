## Building

Just type `make` while you are in the directory.

## Copyright

This work is
Copyright © 2015-2017 Mattia Rizzolo <mattia@mapreri.org>

## License

Everything in this directory, unless otherwise specified, is licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License. For the full
license text see https://creativecommons.org/licenses/by-sa/4.0/legalcode
